package com.tavant.ProducerService.Model;

public class Book {
	private Integer bookId;
    private String bookName;
    private Double bookCost;
	public Integer getBookId() {
		return bookId;
	}
	public String getBookName() {
		return bookName;
	}
	public Double getBookCost() {
		return bookCost;
	}
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public void setBookCost(Double bookCost) {
		this.bookCost = bookCost;
	}
	public Book(Integer bookId, String bookName, Double bookCost) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookCost = bookCost;
	}
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookName=" + bookName + ", bookCost=" + bookCost + "]";
	}
	
    
    
}
